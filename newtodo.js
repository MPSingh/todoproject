var todolist = [],obj = {};
var i, value, li, cbox, TotalItems;

function addlistOnbody(index) {
    var list = document.getElementById("dolist");
    li = document.createElement("li");
    cbox = document.createElement("input");
    var close = document.createElement("i");
    li.setAttribute("class", "item");
    cbox.setAttribute("index", index);
    cbox.setAttribute("type", "checkBox");
    close.setAttribute("class", "fa fa-close");
    cbox.onclick = checktodo.bind(cbox);
    close.onclick = deletetodo.bind(close);

    li.appendChild(cbox);
    li.appendChild(document.createTextNode(value));
    li.appendChild(close);
    li.setAttribute("draggable", true);

    li.setAttribute("ondragstart", "start(event)");
    li.setAttribute("ondragover", "over(event)");
    li.setAttribute("ondrop", "drop(event)"); 
    li.setAttribute("ondragend", "end(event)"); 
    list.appendChild(li);
    document.getElementById("container").appendChild(list);
}

function newtodo() {
    value = document.getElementById("input").value;
    if (value == "") {
        alert("Add to dos");
        document.getElementById("input").focus();
    } else {
        todolist = JSON.parse(localStorage.getItem("todolist"));
        if (todolist == null) {
            todolist = [];
        }
        index = todolist.length;
        addlistOnbody(index);
        storetodo();
    }
}

function storetodo() {
    obj = { "data": value, "check": false, "index": todolist.length };
    todolist.push(obj);
    localStorage.setItem("todolist", JSON.stringify(todolist));
}

function checktodo() {
    if (this.checked) {
        this.parentNode.style.textDecoration = "line-through";
        this.parentNode.style.backgroundColor = "olive";
        this.parentNode.style.color = "white";
        updateChecked(this);
    } else {
        this.parentNode.style.textDecoration = "none";
        this.parentNode.style.backgroundColor = "yellow";
        this.parentNode.style.color = "red";
        updateUnChecked(this);
    }
}

function updateChecked() {
    todolist = JSON.parse(localStorage.getItem("todolist"));
    for (var i = 0; i < todolist.length; i++) {
        if (event.target.getAttribute("index") == todolist[i].index) {
            todolist[i].check = true;
        }
    }
    localStorage.setItem("todolist", JSON.stringify(todolist));
}


function updateUnChecked() {
    todolist = JSON.parse(localStorage.getItem("todolist"));
    for (var i = 0; i < todolist.length; i++) {
        if (event.target.getAttribute("index") == todolist[i].index) {
            todolist[i].check = false;
        }
    }
    localStorage.setItem("todolist", JSON.stringify(todolist));
}

function onloadtodo() {
    todolist = JSON.parse(localStorage.getItem("todolist"));
    if (todolist == null) {
        todolist = [];
    }
    for (var i = 0; i < todolist.length; i++) {
        value = todolist[i].data;
        index = i;
        addlistOnbody(index);
        if (todolist[i].check == true) {
            li.style.textDecoration = "line-through";
            li.firstChild.checked = true;
        }
    }
}

function deletetodo() {
    todolist = JSON.parse(localStorage.getItem("todolist"));
    var parent = event.target.parentNode;

    todolist.splice(parent.firstChild.getAttribute("index"), 1);
    for (var i = 0; i < todolist.length; i++) {
        todolist[i].index = i;
    }
    localStorage.setItem("todolist", JSON.stringify(todolist));
    parent.remove();
}

var targetId, srcId;

function start(event) {
    srcId = event.target.firstChild.getAttribute("index");
}

function over(event) {
    event.preventDefault();
}

function drop(event) {
    targetId = event.target.firstChild.getAttribute("index");
    if (srcId != targetId) {
        todolist = JSON.parse(localStorage.getItem("todolist"));
        var src = todolist.splice(srcId, 1);
        todolist.splice(targetId, 0, src[0]);
        for (var i = 0; i < todolist.length; i++) {
            todolist[i].index = i;
        }
        localStorage.setItem("todolist", JSON.stringify(todolist));
        
    }
}

function end(event) {
    event.preventDefault();
    var Item = document.querySelectorAll("LI");
    var myNode = document.getElementById("dolist");
    while (myNode.firstChild) {
        myNode.removeChild(myNode.firstChild);
    }
    onloadtodo();
}